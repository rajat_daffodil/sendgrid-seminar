# SendGrid Seminar

SendGrid provides a cloud-based email delivery service that assists businesses with email delivery.

### History

The company was founded by Isaac Saldana, Jose Lopez, and Tim Jenkins in 2009. Before Sendgrid was called Sendgrid, they named the project smtpapi.com a domain name still owned by Sendgrid. In December 2012, SendGrid teamed up with Twilio to incorporate SMS and push notification services to its platform.

### Directory Structure

```
sendgrid
│   README.md
│   package.json
│   app.js
│   .gitignore
│   .eslintrc.json
│   .babelrc
│
└───attachments
│   │   ngrok.zip
│   │   sample.csv
│   │   sample.doc
│   │   sample.html
│   │   sample.jpeg
│   │   sample.pdf
│   │   sample.pptx
│   │   sample.xlsx
│
└───config
│   │   apiKey.js
│   │   users.js
│
└───mail
    │   attachment.js
    │   manuallyContent.js
    │   multipleEmailMultipleRecipient.js
    │   sendAt.js
    │   singleEmailMultipleRecipient.js
    │   singleEmailSingleRecipient.js
    │   template.js
```

### Installation

Install the dependencies and devDependencies and start the server (server will be used in webhook).

```sh
$ cd sendgrid-seminar
$ npm install
$ npm start
```

### SendGrid Account

You need to make account on [SendGrid](https://sendgrid.com/) and get your own api key.

After getting your api key you need to paste it in apiKey.js file.

```sh
config -> apiKey.js
```

You also need to change the users.js file and paste some email id.

```sh
config -> users.js
```

### Run

As I make seperate scripts for each file in mail directory to run so you don't need to type whole file name You just need to run script for each file.
You can also run files manually without using scripts.

To run singleEmailSingleRecipient.js file

```sh
$ npm run sesr
```

To run singleEmailMultipleRecipient.js file

```sh
$ npm run semr
```

To run multipleEmailMultipleRecipient.js file

```sh
$ npm run memr
```

To run manuallyContent.js file

```sa
$ npm run manco
```

To run sendAt.js file

```sa
$ npm run sa
```

To run attachment.js file

```sa
$ npm run at
```

To run template.js file

```sa
$ npm run temp
```

### Status Codes

    202: Accepted (Accepted for processing)

    400: Bad Request (e.g from field is missing)

    401: Unauthorized (e.g. Wrong api key)

    403: Forbidden (e.g. When you completed your daily sending email limit)

    413: Payload Too Large

### SendGrid Website Features

Go to [Sendgrid](https://sendgrid.com/) Website

    Activity
    Settings -> IP Access Management
    Settings -> Alert Settings
    Settings -> Mail Settings -> BCC
    Settings -> Mail Settings -> Footer
    Templates -> Transactional
    Settings -> Mail Settings -> Event Notification (Webhook)

### ngrok

In order to test webhook on local server we need ngrok. ngrok is a convenient tool for exposing local servers to publicly accessible URLS, supporting both HTTP and HTTPS. You need to make your own account on [ngrok](https://ngrok.com/). You can find help [here](https://dashboard.ngrok.com/get-started) for creating your account and installing on your system.

In order to start ngrok on your system, you need to run this command

```sh
./ngrok http 1414
```

You can check the incoming request on your server at 4040 port

```sh
http://localhost:4040/
```

### Links

[SendGrid Account](https://sendgrid.com/)
[SendGrid npm](https://www.npmjs.com/package/@sendgrid/mail)
[SendGrid Docs ](https://github.com/sendgrid/sendgrid-nodejs/blob/master/packages/mail/USE_CASES.md)
[SendGrid Docs](https://sendgrid.com/docs/API_Reference/api_v3.html)
[Mobile Version of Gmail](http://m.gmail.com/)
[ngrok](https://ngrok.com/)
[ngrok get started guide](https://dashboard.ngrok.com/get-started)
