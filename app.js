import express from 'express';
import bodyParser from 'body-parser';
import { get, set } from 'lodash';

const app = express();
const port = 1414;

// middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// route
app.post('/sendgrid', (req, res) => {
  const body = get(req, 'body', {});
  let event;
  const emailStatus = {};

  if (get(req, 'body[1].event', null)) {
    event = get(req, 'body[1].event', null);
  } else {
    event = get(req, 'body[0].event', null);
  }

  set(emailStatus, 'sgMessageId', get(req, 'body[0].sg_message_id', null).split('.')[0]);
  set(emailStatus, 'event', event);

  if (event === 'dropped') {
    set(emailStatus, 'reason', get(req, 'body[0].reason', null));
  } else if (event === 'bounce') {
    set(emailStatus, 'reason', get(req, 'body[0].reason', null));
  }

  console.log('Body >>', JSON.stringify(body, null, 2));
  console.log('Email Status: ', JSON.stringify(emailStatus, null, 2));
  res.send({ body });
});

app.listen(port, () => {
  console.log(`Server is working on port ${port}`);
});
