const users = {
  from: 'admin@study.com',
  to: {
    user1: 'xxxxxxxxxxxxxxxx',
    user2: 'xxxxxxxxxxxxxxxx',
    user3: 'xxxxxxxxxxxxxxxx',
    user4: 'xxxxxxxxxxxxxxxx',
    user5: 'xxxxxxxxxxxxxxxx',
    user6: 'xxxxxxxxxxxxxxxx',
  },
};

export default users;
