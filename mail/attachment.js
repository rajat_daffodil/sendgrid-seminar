import sgMail from '@sendgrid/mail';
import fs from 'fs';
import path from 'path';

import apiKey from '../config/apiKey';
import users from '../config/users';

sgMail.setApiKey(apiKey);

const { to, from } = users;
const { user1 } = to;

function base64Encode(file) {
  const filePath = path.join(__dirname, file);
  const bitmap = fs.readFileSync(filePath);
  return Buffer.from(bitmap).toString('base64');
}

const msg = {
  from,
  to: user1,
  subject: 'Attachment',
  text: 'Attachment Text',
  html: '<strong>Attachment HTML</strong>',
  attachments: [
    {
      content: base64Encode('../attachments/sample.jpeg'),
      filename: 'some-attachment.jpg',
    },
    {
      content: base64Encode('../attachments/sample.pdf'),
      filename: 'some-attachment.pdf',
    },
    {
      content: base64Encode('../attachments/sample.doc'),
      filename: 'some-attachment.doc',
    },
    {
      content: base64Encode('../attachments/sample.pptx'),
      filename: 'some-attachment.pptx',
    },
    {
      content: base64Encode('../attachments/sample.xlsx'),
      filename: 'some-attachment.xlsx',
    },
    {
      content: base64Encode('../attachments/sample.csv'),
      filename: 'some-attachment.csv',
    },
    {
      content: base64Encode('../attachments/sample.html'),
      filename: 'some-attachment.html',
    },
    {
      content: base64Encode('../attachments/ngrok.zip'),
      filename: 'some-attachment.zip',
    },
    // {
    //   content: base64Encode('../attachments/ngrok'),
    //   filename: 'some-attachment.exe',
    // },
  ],
};

sgMail
  .send(msg)
  .then((successRes) => {
    console.log('Success <<>>', JSON.stringify(successRes, null, 2));
  })
  .catch((errorRes) => {
    console.log('Error <<>>', JSON.stringify(errorRes, null, 2));
  });
