import sgMail from '@sendgrid/mail';

import apiKey from '../config/apiKey';
import users from '../config/users';

sgMail.setApiKey(apiKey);

const { to, from } = users;
const { user1 } = to;

const msg = {
  from,
  to: user1,
  subject: 'Manual Content',
  content: [
    { type: 'text/plain', value: 'Manual Content Text' },
    { type: 'text/html', value: '<h1>Manual Content HTML</h1>' },
  ],
};

sgMail
  .send(msg)
  .then((successRes) => {
    console.log('Success <<>>', JSON.stringify(successRes, null, 2));
  })
  .catch((errorRes) => {
    console.log('Error <<>>', JSON.stringify(errorRes, null, 2));
  });
