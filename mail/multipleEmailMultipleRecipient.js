import sgMail from '@sendgrid/mail';

import apiKey from '../config/apiKey';
import users from '../config/users';

sgMail.setApiKey(apiKey);

const { to, from } = users;
const { user1, user2 } = to;

const emails = [
  {
    from,
    to: user1,
    subject: 'Multiple Email Multiple Recipient Subject 1',
    text: 'Multiple Email Multiple Recipient Text 1',
    html: '<h1>Multiple Email Multiple Recipient HTML 1</h1>',
  },
  {
    from,
    to: user2,
    subject: 'Multiple Email Multiple Recipient Subject 2',
    text: 'Multiple Email Multiple Recipient Text 2',
    html: '<h1>Multiple Email Multiple Recipient HTML 2</h1>',
  },
];

sgMail
  .send(emails)
  .then((successRes) => {
    console.log('Success <<>>', JSON.stringify(successRes, null, 2));
  })
  .catch((errorRes) => {
    console.log('Error <<>>', JSON.stringify(errorRes, null, 2));
  });
