import sgMail from '@sendgrid/mail';

import apiKey from '../config/apiKey';
import users from '../config/users';

sgMail.setApiKey(apiKey);

const { to, from } = users;
const { user1 } = to;

const msg = {
  from,
  to: user1,
  subject: 'Send At',
  text: 'Send At Text',
  html: '<h1>Send At HTML</h1>',
  sendAt: parseInt(new Date().getTime() / 1000, 10) + 1 * 60, // 72 hours max
};

sgMail
  .send(msg)
  .then((successRes) => {
    console.log('Success <<>>', JSON.stringify(successRes, null, 2));
  })
  .catch((errorRes) => {
    console.log('Error <<>>', JSON.stringify(errorRes, null, 2));
  });
