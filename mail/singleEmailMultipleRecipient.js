import sgMail from '@sendgrid/mail';

import apiKey from '../config/apiKey';
import users from '../config/users';

sgMail.setApiKey(apiKey);

const { to, from } = users;
const {
  user1, user2, user3, user4, user5, user6,
} = to;

const msg = {
  from,
  to: [user1, user2],
  cc: [user3, user4],
  bcc: [user5, user6],
  replyTo: from,
  subject: 'Single Email Multiple Recipient',
  text: 'Single Email Multiple Recipient',
  html: '<h1>Single Email Multiple Recipient</h1>',
};

sgMail
  .send(msg) // users present in 'to' field can see each other name in 'to'
  // .sendMultiple(msg) // users present in 'to' field cannot see each other name in 'to'
  .then((successRes) => {
    console.log('Success <<>>', JSON.stringify(successRes, null, 2));
  })
  .catch((errorRes) => {
    console.log('Error <<>>', JSON.stringify(errorRes, null, 2));
  });
