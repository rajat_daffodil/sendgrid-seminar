import sgMail from '@sendgrid/mail';

import apiKey from '../config/apiKey';
import users from '../config/users';

sgMail.setApiKey(apiKey);

const { to, from } = users;
const { user1 } = to;

const msg = {
  from,
  // from: 'Admin <admin@study.com>',
  to: user1,
  // to: 'Rajat Kumar <rajat.kumar@daffodilsw.com>',
  // to: {email:user1,name:'Rajat Kumar'},
  // to: 'rajat.kumar@daffodilsw.commmmmmmmmmmmmmmmmmmmmmmmmmmmm',
  // to: 'rajat.kumarrrrrrrrrrrrrrrrrrrrrrrrrrrrr@daffodilsw.com',
  subject: 'Single Email Single Recipient',
  text: 'Single Email Single Recipient Text',
  html:
    "<h1>Single Email Single Recipient HTML</h1><a href='www.google.co.in'>Click me</a> to go to Google",
};

sgMail
  .send(msg)
  .then((successRes) => {
    console.log('Success <<>>', JSON.stringify(successRes, null, 2));
  })
  .catch((errorRes) => {
    console.log('Error <<>>', JSON.stringify(errorRes, null, 2));
  });
