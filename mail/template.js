import sgMail from '@sendgrid/mail';

import apiKey from '../config/apiKey';
import users from '../config/users';

sgMail.setApiKey(apiKey);

const { to, from } = users;
const { user1 } = to;

const msg = {
  from,
  to: user1,
  subject: 'Template',
  text: 'Template Text',
  html: '<h1>Template HTML</h1>',
  templateId: 'd-4b2be907a0504f89986e230bf0cf3f71', // Change this to your own template id
  dynamic_template_data: {
    subject: 'Email and Password',
    email: 'abc@gmail.com',
    password: '123456',
    imageSrc: 'https://cdn-images-1.medium.com/max/1200/1*ztA8nOlyU3QRMRYlx3vJ8A.png',
  },
};

sgMail
  .send(msg)
  .then((successRes) => {
    console.log('Success <<>>', JSON.stringify(successRes, null, 2));
  })
  .catch((errorRes) => {
    console.log('Error <<>>', JSON.stringify(errorRes, null, 2));
  });
